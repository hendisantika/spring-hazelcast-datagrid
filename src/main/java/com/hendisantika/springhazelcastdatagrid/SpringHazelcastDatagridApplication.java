package com.hendisantika.springhazelcastdatagrid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHazelcastDatagridApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringHazelcastDatagridApplication.class, args);
    }
}

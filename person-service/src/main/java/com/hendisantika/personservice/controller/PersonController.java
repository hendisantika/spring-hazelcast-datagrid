package com.hendisantika.personservice.controller;

import com.hendisantika.personservice.model.Person;
import com.hendisantika.personservice.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-datagrid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/02/18
 * Time: 20.34
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class PersonController {

    protected Logger logger = Logger.getLogger(PersonController.class.getName());

    @Autowired
    PersonRepository repository;

    @Autowired
    CacheManager manager;

    @PostConstruct
    public void init() {
        logger.info("Cache manager: " + manager);
        logger.info("Cache manager names: " + manager.getCacheNames());
    }

    @GetMapping("/persons/pesel/{pesel}")
    public List<Person> findByPesel(@PathVariable("pesel") String pesel) {
        return repository.findByPesel(pesel);
    }

    @GetMapping("/persons/{id}")
    public Person findById(@PathVariable("id") Integer id) {
        return repository.findOne(id);
    }

    @GetMapping("/persons")
    public List<Person> findAll() {
        return (List<Person>) repository.findAll();
    }
}

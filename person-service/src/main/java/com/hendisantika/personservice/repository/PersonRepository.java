package com.hendisantika.personservice.repository;

import com.hendisantika.personservice.model.Person;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-datagrid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/02/18
 * Time: 19.57
 * To change this template use File | Settings | File Templates.
 */
public interface PersonRepository extends CrudRepository<Person, Integer> {

    @Cacheable("findByPesel")
    List<Person> findByPesel(String pesel);

}

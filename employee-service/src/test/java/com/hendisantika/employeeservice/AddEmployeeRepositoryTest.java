package com.hendisantika.employeeservice;

import com.hendisantika.employeeservice.model.Employee;
import com.hendisantika.employeeservice.repository.EmployeeRepository;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-datagrid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/09/18
 * Time: 20.47
 * To change this template use File | Settings | File Templates.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AddEmployeeRepositoryTest {

    protected Logger logger = Logger.getLogger(AddEmployeeRepositoryTest.class.getName());

    @Autowired
    EmployeeRepository repository;

    @Test
    public void add() {
        for (int i = 0; i < 2000000; i++) {
            int ix = new Random().nextInt(100);
            Employee e = new Employee();
            e.setPersonId(i);
            e.setCompany("TEST" + new DecimalFormat("000").format(ix));
            e = repository.save(e);
            logger.info("Add: " + e);
        }
    }

}
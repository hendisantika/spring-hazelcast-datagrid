package com.hendisantika.employeeservice.controller;

import com.hendisantika.employeeservice.model.Employee;
import com.hendisantika.employeeservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-datagrid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/02/18
 * Time: 06.27
 * To change this template use File | Settings | File Templates.
 */

@RestController
public class EmployeeController {

    private Logger logger = Logger.getLogger(EmployeeController.class.getName());

    @Autowired
    EmployeeService service;

    @GetMapping("/employees/person/{id}")
    public Employee findByPersonId(@PathVariable("id") Integer personId) {
        logger.info(String.format("findByPersonId(%d)", personId));
        return service.findByPersonId(personId);
    }

    @GetMapping("/employees/company/{company}")
    public List<Employee> findByCompany(@PathVariable("company") String company) {
        logger.info(String.format("findByCompany(%s)", company));
        return service.findByCompany(company);
    }

    @GetMapping("/employees/{id}")
    public Employee findById(@PathVariable("id") Integer id) {
        logger.info(String.format("findById(%d)", id));
        return service.findById(id);
    }

    @PostMapping("/employees")
    public Employee add(@RequestBody Employee emp) {
        logger.info(String.format("add(%s)", emp));
        return service.add(emp);
    }

}

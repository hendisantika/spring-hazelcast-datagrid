package com.hendisantika.employeeservice.repository;

import com.hendisantika.employeeservice.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-datagrid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/02/18
 * Time: 23.47
 * To change this template use File | Settings | File Templates.
 */

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    Employee findByPersonId(Integer personId);

    List<Employee> findByCompany(String company);

}
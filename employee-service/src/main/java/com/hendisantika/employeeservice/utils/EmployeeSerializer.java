package com.hendisantika.employeeservice.utils;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;
import com.hendisantika.employeeservice.model.Employee;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-hazelcast-datagrid
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/02/18
 * Time: 23.48
 * To change this template use File | Settings | File Templates.
 */

public class EmployeeSerializer implements StreamSerializer<Employee> {

    @Override
    public int getTypeId() {
        return 1;
    }

    @Override
    public void write(ObjectDataOutput out, Employee employee) throws IOException {
        out.writeInt(employee.getId());
        out.writeInt(employee.getPersonId());
        out.writeUTF(employee.getCompany());
    }

    @Override
    public Employee read(ObjectDataInput in) throws IOException {
        Employee e = new Employee();
        e.setId(in.readInt());
        e.setPersonId(in.readInt());
        e.setCompany(in.readUTF());
        return e;
    }

    @Override
    public void destroy() {
    }

}